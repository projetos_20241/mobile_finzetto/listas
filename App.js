import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from "./src/homeScreen";
import UserScreen from "./src/userScreen";
import GestosScreen from "./src/gestosScreen";


const Stack = createStackNavigator();


export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Users" component={UserScreen} />
        <Stack.Screen name="Gestos" component={GestosScreen} /> 
      </Stack.Navigator>

    </NavigationContainer>
  );
}




