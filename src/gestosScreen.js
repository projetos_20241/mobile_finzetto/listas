import React, { useEffect, useState } from "react";
import { View, Text, PanResponder, StyleSheet, Dimensions } from "react-native";

const GestosScreen = ({ navigation }) => {

    const ScreenWidth = Dimensions.get('window').width;

    const panResponder = React.useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gestureState) => {
                console.log('Movimento X', gestureState.dx)
                console.log('Movimento Y', gestureState.dy)
            },
            onPanResponderRelease: (event, gestureState) => {
                if (gestureState.dx > ScreenWidth / 2) {
                    navigation.goBack();
                }
            },
        })
    ).current;

    return (
        <View style={styles.container} {...panResponder.panHandlers}>
            <Text style={styles.text}>Arraste para direita</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    text: {
        fontSize: 20, 
        fontWeight: 'bold'
    },
});

export default GestosScreen;
